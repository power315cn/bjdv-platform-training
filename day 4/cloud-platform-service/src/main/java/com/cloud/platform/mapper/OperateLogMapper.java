package com.cloud.platform.mapper;

import com.cloud.platform.model.OperateLog;

import java.util.List;

/**
 * 操作日志 mapper
 *
 * @Author: 程楠
 * @Date: 2021/7/16
 * @Description:
 */
public interface OperateLogMapper {

    /**
     * 添加日志
     *
     * @param operateLog
     */
    void addOperateLog(OperateLog operateLog);

    /**
     * 查询日志记录
     *
     * @return
     */
    List<OperateLog> listOperateLog();

    /**
     * 删除日志
     */
    int deleteOperateLog(String time);

}
