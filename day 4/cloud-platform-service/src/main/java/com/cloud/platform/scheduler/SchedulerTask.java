package com.cloud.platform.scheduler;

import com.cloud.platform.service.OperateLogService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * 定时任务
 *
 * @author 程楠
 * @date 2021/7/14
 */
@Component
public class SchedulerTask {

    private static final Logger log = LoggerFactory.getLogger(SchedulerTask.class);
    private int count = 0;


    @Autowired
    private OperateLogService operateLogService;

    /**
     * 每10s执行一次
     */
    @Scheduled(cron = "*/10 * * * * ?")
    private void process() {
        log.info("定时任务开始， {}s", 10 * count++);
    }


    /**
     * 每10分钟执行一次
     */
    @Scheduled(cron = "*/10 * * * * ?")
    // @Scheduled(cron = "* */10 * * * ?")
    private void deleteOperateLog() {
        String time = LocalDateTime.now().minusHours(1).format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        log.info("删除1个小时前日志, time: {}", time);
        int count = operateLogService.deleteOperateLog(time);
        log.info("删除1个小时前日志, 删除数量: {}", count);
    }

}