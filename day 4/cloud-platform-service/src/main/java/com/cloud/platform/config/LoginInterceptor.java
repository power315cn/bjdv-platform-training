package com.cloud.platform.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.AsyncHandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * 登录拦截器
 *
 * @author 程楠
 * @date 2021/7/13
 */
@Component
public class LoginInterceptor implements AsyncHandlerInterceptor {
    private static final Logger log = LoggerFactory.getLogger(LoginInterceptor.class);

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String token = request.getHeader("token");
        log.info("请求开始, url: {}, token: {}", request.getRequestURL(), token);
        if (token == null || token.isEmpty()) {
            throw new RuntimeException("token不能为空");
        }
        String username = LoginCache.getCache(token);
        if (username == null || username.isEmpty()) {
            throw new RuntimeException("用户未登录");
        }

        return true;
    }

    @Override
    public void postHandle(HttpServletRequest arg0, HttpServletResponse arg1, Object arg2, ModelAndView arg3) throws Exception {
    }

    @Override
    public void afterCompletion(HttpServletRequest arg0, HttpServletResponse response, Object arg2, Exception arg3) throws Exception {
    }

}
