package com.cloud.platform.service;

import com.cloud.platform.config.LoginCache;
import com.cloud.platform.mapper.UserMapper;
import com.cloud.platform.model.User;
import com.cloud.platform.model.UserQuery;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * 用户服务
 *
 * @Author: 程楠
 * @Date: 2021/7/13
 * @Description:
 */
@Service
public class UserService {

    private static final Logger log = LoggerFactory.getLogger(UserService.class);

    @Autowired
    private UserMapper userMapper;

    /**
     * 登录方法
     *
     * @param username 用户名
     * @param password 密码
     */
    public String login(String username, String password) {
        User user = userMapper.getUserByUsername(username);
        if (user == null) {
            log.error("用户不存在, username: {}", username);
            throw new RuntimeException("用户不存在");
        }

        if (!password.equals(user.getPassword())) {
            log.error("密码不正确, password: {}", password);
            throw new RuntimeException("密码不正确");
        }
        String token = System.currentTimeMillis() + new Random().nextInt(1000000) + "";
        LoginCache.setCache(token, username);
        return token;
    }

    /**
     * 查询用户列表
     *
     * @param userQuery 查询条件
     */
    public List<User> listUser(UserQuery userQuery) {
        return userMapper.listUser(userQuery);
    }

    /**
     * 查询用户记录
     *
     * @param id 用户主键
     */
    public User getUser(Integer id) {
        return userMapper.getUserById(id);
    }

    /**
     * 添加用户
     *
     * @param user 用户对象
     */
    public void addUser(User user) {
        User existUser = userMapper.getUserByUsername(user.getUsername());
        // 判断用户名是否已经使用了
        if (existUser != null) {
            throw new RuntimeException("用户名已存在");
        }
        Date now = new Date();
        user.setCreateTime(now);
        user.setUpdateTime(now);
        userMapper.addUser(user);
    }

    /**
     * 更新用户
     *
     * @param user 用户对象
     */
    public void updateUser(User user) {
        User existUser = userMapper.getUserById(user.getId());
        // 判断用户是否存在
        if (existUser == null) {
            throw new RuntimeException("用户不存在");
        }
        Date now = new Date();
        user.setUpdateTime(now);
        userMapper.updateUserById(user);
    }

    /**
     * 删除用户
     *
     * @param id 用户主键
     */
    public void deleteUser(Integer id) {
        userMapper.deleteUserById(id);
    }
}
