package com.cloud.platform.model;

import java.util.Date;

/**
 * 操作日志实体对象
 *
 * @Author: 程楠
 * @Date: 2021/7/16
 * @Description:
 */
public class OperateLog {
    /**
     * 主键id
     */
    private Integer id;

    /**
     * 操作：查询，分页，新增，修改，删除，登录，注销'
     */
    private String action;

    /**
     * 操作内容
     */
    private String content;

    /**
     * 操作人
     */
    private String operator;

    /**
     * 操作时间
     */
    private Date operateTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getAction() {
        return action;
    }

    public void setAction(String action) {
        this.action = action;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Date getOperateTime() {
        return operateTime;
    }

    public void setOperateTime(Date operateTime) {
        this.operateTime = operateTime;
    }
}

