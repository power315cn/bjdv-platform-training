package com.cloud.platform.controller;

import com.cloud.platform.model.User;
import com.cloud.platform.model.UserQuery;
import com.cloud.platform.service.OperateLogService;
import com.cloud.platform.service.UserService;
import com.cloud.platform.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * 用户控制器接口
 *
 * @Author: 程楠
 * @Date: 2021/7/13
 * @Description:
 */
@RestController
public class UserController {

    private static final Logger log = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @Autowired
    private OperateLogService operateLogService;

    // @RequestMapping("/getUser")
    // public User getUser() {
    //     User user = new User();
    //     user.setName("小爱同学");
    //     user.setPassword("123456");
    //     return user;
    // }

    /**
     * 登录接口
     *
     * @param username 用吗
     * @param password 密码
     * @return
     */
    @GetMapping("/login")
    public Response login(@RequestParam String username, @RequestParam String password) {
        log.info("用户登录, 用户名: {}, 密码: {}", username, password);
        operateLogService.addOperateLog("登录", "用户" + username);
        String token = userService.login(username, password);
        return new Response(1, "成功", token);
    }

    /**
     * 查询用户列表
     *
     * @param userQuery
     * @return
     */
    @GetMapping("/listUser")
    public Response list(UserQuery userQuery) {
        log.info("查询用户列表, 查询条件: {}", userQuery);
        operateLogService.addOperateLog("查询列表", "用户userQuery=" + userQuery);
        List<User> list = userService.listUser(userQuery);
        return new Response(1, "成功", list);
    }

    /**
     * 查询用户记录
     *
     * @param id
     * @return
     */
    @GetMapping("/getUser")
    public Response getUser(@RequestParam Integer id) {
        log.info("查询用户记录, 用户id: {}", id);
        operateLogService.addOperateLog("查询记录", "用户id=" + id);
        User user = userService.getUser(id);
        return new Response(1, "成功", user);
    }

    /**
     * 添加用户
     *
     * @param user
     * @return
     */
    @PostMapping("/addUser")
    public Response addUser(@RequestBody User user) {
        Assert.notNull(user.getUsername(), "username不能为空");
        Assert.notNull(user.getPassword(), "password不能为空");
        Assert.notNull(user.getName(), "name不能为空");
        operateLogService.addOperateLog("添加", "用户user=" + user);
        userService.addUser(user);
        return new Response(1, "成功", null);
    }

    /**
     * 更新用户
     *
     * @param user
     * @return
     */
    @PostMapping("/updateUser")
    public Response updateUser(@RequestBody User user) {
        Assert.notNull(user.getId(), "id不能为空");
        operateLogService.addOperateLog("更新", "用户user=" + user);
        userService.updateUser(user);
        return new Response(1, "成功", null);
    }

    /**
     * 删除用户
     *
     * @param id
     * @return
     */
    @GetMapping("/deleteUser")
    public Response deleteUser(@RequestParam Integer id) {
        operateLogService.addOperateLog("删除", "用户id=" + id);
        userService.deleteUser(id);
        return new Response(1, "成功", null);
    }

    /**
     * 注销用户
     *
     * @param id
     * @return
     */
    @GetMapping("/cancelUser")
    public Response cancelUser(@RequestParam Integer id) {
        operateLogService.addOperateLog("注销", "用户id" + id);
        userService.deleteUser(id);
        return new Response(1, "成功", null);
    }
}
