package com.cloud.platform.mapper;

import com.cloud.platform.model.User;
import com.cloud.platform.model.UserQuery;

import java.util.List;

/**
 * 用户 mapper
 *
 * @Author: 程楠
 * @Date: 2021/7/13
 * @Description:
 */
public interface UserMapper {
    /**
     * 登录返回用户
     *
     * @param username 用户名
     */
    User getUserByUsername(String username);

    /**
     * 根据条件查询用户列表
     *
     * @param userQuery
     * @return
     */
    List<User> listUser(UserQuery userQuery);

    /**
     * 查询用户记录
     *
     * @param id
     * @return
     */
    User getUserById(Integer id);

    /**
     * 添加用户
     *
     * @param user 用户对象
     */
    void addUser(User user);

    /**
     * 更新用户
     *
     * @param user 用户对象
     */
    void updateUserById(User user);

    /**
     * 删除用户
     *
     * @param id 用户主键
     */
    void deleteUserById(Integer id);
}
