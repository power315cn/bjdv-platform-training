package com.cloud.platform.controller;

import com.cloud.platform.model.OperateLog;
import com.cloud.platform.service.OperateLogService;
import com.cloud.platform.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * 日志控制器接口
 *
 * @Author: 程楠
 * @Date: 2021/7/13
 * @Description:
 */
@RestController
public class OperateLogController {

    private static final Logger log = LoggerFactory.getLogger(OperateLogController.class);

    @Autowired
    private OperateLogService operateLogService;

    /**
     * 查询操作日志列表
     *
     * @return
     */
    @GetMapping("/listOperateLog")
    public Response listOperateLog() {
        List<OperateLog> list = operateLogService.listOperateLog();
        return new Response(1, "成功", list);
    }

}
