package com.cloud.platform.config;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;

/**
 * 登录缓存
 *
 * @Author: 程楠
 * @Date: 2021/7/13
 * @Description:
 */
@Component
@Scope(value = "singleton")
public class LoginCache {

    private static Map<String, String> CACHE = new HashMap<>();

    public static String getCache(String token) {
        return CACHE.get(token);
    }

    public static void setCache(String token, String username) {
        CACHE.put(token, username);
    }
}
