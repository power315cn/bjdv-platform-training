package com.cloud.platform.utils;

/**
 * @author 程楠
 * @date 2021/7/13
 * @description 统一返回结果的格式工具类
 */
public class Response {
    /**
     * 状态码,1表示成功，0表示失败
     */
    private Integer code;

    /**
     * 提示信息
     */
    private String message;

    /**
     * 返回包装类型
     */
    private Object data;

    private Response() {
    }

    public Response(Integer code, String message, Object data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Integer getCode() {
        return code;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }
}
