package com.cloud.platform.error;

import com.cloud.platform.utils.Response;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 全局异常处理
 *
 * @author 程楠
 * @date 2021/7/13
 */
@ControllerAdvice
@ResponseBody
public class ExceptionTranslator {
    private static final Logger log = LoggerFactory.getLogger(ExceptionTranslator.class);

    @ExceptionHandler({Exception.class})
    public Response handleException(Exception e) {
        log.error("全局异常, 消息:{}", e.getMessage(), e);
        return new Response(0, e.getMessage(), null);
    }

}
