package com.cloud.platform.service;

import com.cloud.platform.mapper.OperateLogMapper;
import com.cloud.platform.model.OperateLog;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

/**
 * 操作日志 服务
 *
 * @Author: 程楠
 * @Date: 2021/7/16
 * @Description:
 */
@Service
public class OperateLogService {

    @Autowired
    private OperateLogMapper operateLogMapper;

    /**
     * 添加日志
     *
     * @param content
     */
    public void addOperateLog(String action, String content) {
        OperateLog operateLog = new OperateLog();
        operateLog.setAction(action);
        operateLog.setContent(content);
        operateLog.setOperator("系统");
        operateLog.setOperateTime(new Date());
        operateLogMapper.addOperateLog(operateLog);
    }

    /**
     * 查询日志记录
     *
     * @return
     */
    public List<OperateLog> listOperateLog() {
        return operateLogMapper.listOperateLog();
    }

    /**
     * 删除日志
     *
     * @param time
     */
    public int deleteOperateLog(String time) {
       return operateLogMapper.deleteOperateLog(time);
    }
}
