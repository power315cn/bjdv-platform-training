1、登录接口
http://localhost:8080/login?username=admin&password=123456

GET

{
    "code": 1,
    "message": "成功",
    "data": "1626238628110"
}

2、查询用户记录
http://localhost:8080/getUser?id=1

GET

header: token

{
    "code": 1,
    "message": "成功",
    "data": {
        "id": 1,
        "username": "admin",
        "password": "123456",
        "name": "管理员",
        "createTime": "2021-07-14 11:07:10",
        "updateTime": "2021-07-14 11:07:13"
    }
}

3、添加用户
http://localhost:8080/addUser

POST

header: token

{
    "username": "test",
    "password": "123456",
    "name": "测试人员"
}

{
    "code": 1,
    "message": "成功",
    "data": null
}

4、修改用户
http://localhost:8080/updateUser

POST

header: token

{
    "username": "test",
    "password": "123456",
    "name": "测试人员"
}

{
    "id": 3,
    "password": "123456",
    "name": "测试人员"
}


5、删除
http://localhost:8080/deleteUser?id=3

GET

header: token

{
    "code": 1,
    "message": "成功",
    "data": null
}

6、查询用户列表
http://localhost:8080/listUser?username=admin&name=管理员

GET

header: token

{
    "code": 1,
    "message": "成功",
    "data": [
        {
            "id": 1,
            "username": "admin",
            "password": "123456",
            "name": "管理员",
            "createTime": "2021-07-14 11:07:10",
            "updateTime": "2021-07-14 11:07:13"
        },
        {
            "id": 4,
            "username": "test",
            "password": "123456",
            "name": "测试人员",
            "createTime": "2021-07-14 13:58:28",
            "updateTime": "2021-07-14 13:58:28"
        }
    ]
}