-- 用户表
CREATE TABLE `user` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`username` VARCHAR ( 32 ) NOT NULL COMMENT '用户名',
	`password` VARCHAR ( 32 ) NOT NULL COMMENT '密码',
	`name` VARCHAR ( 32 ) NOT NULL COMMENT '姓名',
	`create_time` datetime DEFAULT NULL COMMENT '创建时间',
	`update_time` datetime DEFAULT NULL COMMENT '修改时间',
	PRIMARY KEY ( `id` ) USING BTREE,
    UNIQUE KEY `UQ_username` ( `username` ) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;

-- 操作日志表
CREATE TABLE `operate_log` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`action` VARCHAR ( 32 ) NOT NULL COMMENT '操作：查询，分页，新增，修改，删除，登录，注销',
	`content` VARCHAR ( 128 ) NOT NULL COMMENT '操作内容',
	`operator` VARCHAR ( 32 ) NOT NULL COMMENT '操作人',
	`operate_time` datetime DEFAULT NULL COMMENT '操作时间',
	PRIMARY KEY ( `id` ) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;