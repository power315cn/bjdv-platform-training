package com.iot.platform.util;

import org.openscada.opc.lib.common.ConnectionInformation;

/**
 * @desc:   连接信息
 * @author: michael
 * @date:2021/7/19 15:15
 */
public class OpcConnection {

    private static final ConnectionInformation CONNECTIONINFORMATION = new ConnectionInformation();

    private OpcConnection() {
    }

    static {
        CONNECTIONINFORMATION.setHost("127.0.0.1");
        CONNECTIONINFORMATION.setProgId("Kepware.KEPServerEX.V6");
        CONNECTIONINFORMATION.setUser("OPCUA");
        CONNECTIONINFORMATION.setPassword("michael123456789");
        CONNECTIONINFORMATION.setClsid("7BC0CC8E-482C-47CA-ABDC-0FE7F9C6E729");
    }

    public static ConnectionInformation getInfo() {
        return CONNECTIONINFORMATION;
    }
}
