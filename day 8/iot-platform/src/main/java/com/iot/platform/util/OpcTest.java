package com.iot.platform.util;

import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.DuplicateGroupException;
import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Server;

import java.net.UnknownHostException;
import java.util.*;
import java.util.concurrent.Executors;

/**
 * @desc:
 * @author: michael
 * @date:2021/7/19 15:56
 */
public class OpcTest {

    public static void main(String[] args) {

        getData();
    }

    private static final List<String> ITEMS = Arrays.asList("JDSimulate.JDAOTPlc.AOT.FengXi", "JDSimulate.JDAOTPlc.AOT.Result",
            "JDSimulate.JDAOTPlc.AOT.Result1", "JDSimulate.JDAOTPlc.AOT.Run_Statue", "JDSimulate.JDAOTPlc.AOT.XigaoLiang");

    public static Map<String,String> getData() {

        Server server = new Server(OpcConnection.getInfo(), Executors.newSingleThreadScheduledExecutor());
        Map<String,String> result = new HashMap<String,String>();
        try {
            server.connect();
            Group group = server.addGroup();
            result = OpcUtil.readValuesMap(group,ITEMS);
        } catch (JIException |  NotConnectedException | DuplicateGroupException | UnknownHostException | AlreadyConnectedException e) {
            e.printStackTrace();
        } finally {
            server.disconnect();
        }
        result.forEach((k,v)->{
            System.out.println(k+"@@"+v);
        });
        return result;
    }
}
