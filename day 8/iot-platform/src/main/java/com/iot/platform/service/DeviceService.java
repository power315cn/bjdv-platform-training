package com.iot.platform.service;

import com.iot.platform.mapper.DeviceMapper;
import com.iot.platform.model.Device;
import com.iot.platform.model.DeviceDetail;
import com.iot.platform.model.DeviceQuery;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @desc:   设备管理服务类
 * @author: michael
 * @date:2021/7/15 16:29
 */
@Service
public class DeviceService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceService.class);

    @Autowired
    private DeviceMapper deviceMapper;

    /**
     * 创建设备
     * @param device
     * @return
     */
    public Long addDevice(Device device){
        LOGGER.info("新增设备[addDevice]<START>,参数信息为:[{}]",device);
        // 检查参数
        checkParam(device);
        //判断编码是否存在
        if(isExistDeviceCode(device.getDeviceCode())){
            throw new RuntimeException("设备编码已存在!");
        }
        //新增
        deviceMapper.addDevice(device);
        LOGGER.info("新增设备[addDevice]<END>,对象ID为:[{}]",device.getId());
        return device.getId();
    }



    /**
     * 修改设备
     * @param device
     * @return
     */
    public Long updateDevice(Device device){
        LOGGER.info("修改设备[updateDevice]<START>,参数信息为:[{}]",device);
        //检查参数
        checkParam(device);
        Device oldDevice = this.getByDeviceCode(device.getDeviceCode());
        device.setId(oldDevice.getId());
        device.setVersion(oldDevice.getVersion());
        deviceMapper.updateDevice(device);
        LOGGER.info("修改设备[updateDevice]<END>,对象ID为:[{}]",device.getId());
        return device.getId();
    }

    /**
     * 判断是否设备编码存在
     * @param deviceCode
     * @return
     */
    public Boolean isExistDeviceCode(String deviceCode){
        LOGGER.info("判断是否设备编码存在[isExistDeviceCode]<START>,参数信息为:[{}]",deviceCode);
        Device device = this.getByDeviceCode(deviceCode);
        return null != device;
    }

    /**
     * 根据参数查询设备列表
     * @param deviceQuery   设备查询参数
     * @return
     */
    public List<Device> getListByQuery(DeviceQuery deviceQuery){
        LOGGER.info("根据参数查询设备列表[getListByQuery]<START>,参数信息为:[{}]",deviceQuery);
        List<Device> deviceList = deviceMapper.listDevice(deviceQuery);
        return deviceList;
    }

    /**
     * 根据设备编码删除对象
     * @param deviceCode    设备编码
     */
    public void delete(String deviceCode){
        deviceMapper.deleteDevice(deviceCode);
    }

    /**
     * 根据编码查询对象
     * @param deviceCode    编码
     * @return
     */
    public Device getByDeviceCode(String deviceCode){
        LOGGER.info("根据编码查询对象[getByDeviceCode]<START>,参数信息为:[{}]",deviceCode);
        if(Strings.isBlank(deviceCode)){
            throw new RuntimeException("设备编码不能为空");
        }
        return deviceMapper.getByDeviceCode(deviceCode);
    }

    /**
     * 根据条件查询详情
     * @param deviceQuery
     * @return
     */
    public List<DeviceDetail> listDeviceDetail(DeviceQuery deviceQuery){
        LOGGER.info("根据条件查询对象[listDeviceDetail]<START>,参数信息为:[{}]",deviceQuery);
        return deviceMapper.listDeviceDetail(deviceQuery);
    }

    private void checkParam(Device device) {
        if(null == device){
            throw new RuntimeException("设备信息不能为空");
        }
        if(Strings.isBlank(device.getDeviceCode())){
            throw new RuntimeException("设备编码不能为空");
        }
        if(Strings.isBlank(device.getDeviceName())){
            throw new RuntimeException("设备名称不能为空");
        }
        if(Strings.isBlank(device.getDeviceIp())){
            throw new RuntimeException("设备IP不能为空");
        }
        if(null == device.getSslType()){
            throw new RuntimeException("协议类型不能为空");
        }
        if(null == device.getDevicePort()){
            throw new RuntimeException("设备端口不能为空");
        }
    }

}
