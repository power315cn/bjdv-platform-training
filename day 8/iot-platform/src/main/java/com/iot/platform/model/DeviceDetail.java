package com.iot.platform.model;

import java.util.List;

/**
 * @desc:   设备详细信息
 * @author: michael
 * @date:2021/7/19 17:11
 */
public class DeviceDetail {

    /**
     * 主键
     */
    private Long id;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备信息
     */
    private List<DeviceCaputerPointAndValue> deviceCaputerPointAndValues;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public List<DeviceCaputerPointAndValue> getDeviceCaputerPointAndValues() {
        return deviceCaputerPointAndValues;
    }

    public void setDeviceCaputerPointAndValues(List<DeviceCaputerPointAndValue> deviceCaputerPointAndValues) {
        this.deviceCaputerPointAndValues = deviceCaputerPointAndValues;
    }

    @Override
    public String toString() {
        return "DeviceDetail{" +
                "id=" + id +
                ", deviceCode='" + deviceCode + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", deviceCaputerPointAndValues=" + deviceCaputerPointAndValues +
                '}';
    }
}
