package com.iot.platform.util;

import org.jinterop.dcom.common.JIException;
import org.jinterop.dcom.core.JIVariant;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.ConnectionInformation;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.*;

import java.net.UnknownHostException;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * @desc:   kepware工具类
 * @author: michael
 * @date:2021/7/16 14:12
 */
public class UtgardTutorialUtil {

    /**
     * 连接
     */
    public static ConnectionInformation connection(){
        final ConnectionInformation connectionInformation = new ConnectionInformation();
        connectionInformation.setHost("127.0.0.1");
        connectionInformation.setProgId("Kepware.KEPServerEX.V6");
        connectionInformation.setUser("OPCUA");
        connectionInformation.setPassword("michael123456789");
        connectionInformation.setClsid("7BC0CC8E-482C-47CA-ABDC-0FE7F9C6E729");
        return connectionInformation;
    }

    public static void readValue(ConnectionInformation connectionInformation,String item) throws Exception{
        //连接对象
        final Server server = new Server(connectionInformation, Executors.newSingleThreadScheduledExecutor());
        try {
            //建立连接
            server.connect();
            //添加一个组
            Group group = server.addGroup();
            //将标记添加到组里
            Item addItem = group.addItem(item);
            //读取标记的值
            JIVariant variant = addItem.read(true).getValue();
            //获取string类型的值
            String value = variant.getObjectAsBoolean()+"";
            System.out.println("读到值：" + value);
        } catch (UnknownHostException | AlreadyConnectedException | JIException |
                NotConnectedException | DuplicateGroupException | AddFailedException e) {
            e.printStackTrace();
        } finally {
            //关闭连接
            server.disconnect();
        }
    }

    public static void main(String[] args) {
        //连接对象
        final Server server = new Server(connection(), Executors.newSingleThreadScheduledExecutor());
        try {
            //建立连接
            server.connect();
            //添加一个组
            Group group = server.addGroup();
            //将标记添加到组里
            Item addItem = group.addItem("JDSimulate.JDAOTPlc.AOT.Result");
            //读取标记的值
            JIVariant variant = addItem.read(true).getValue();
            //获取string类型的值
            String value = variant.getObjectAsBoolean()+"";
            System.out.println("读到值：" + value);
        } catch (UnknownHostException | AlreadyConnectedException | JIException |
                NotConnectedException | DuplicateGroupException | AddFailedException e) {
            e.printStackTrace();
        } finally {
            //关闭连接
            server.disconnect();
        }
    }

    public static  void readValueSchedule(ConnectionInformation connectionInformation,String itemId){
        //连接对象
        final Server server = new Server(connectionInformation, Executors.newSingleThreadScheduledExecutor());
        try {
            // connect to server，连接到服务
            server.connect();

            // add sync access, poll every 500 ms，启动一个同步的access用来读取地址上的值，线程池每500ms读值一次
            // 这个是用来循环读值的，只读一次值不用这样
            final AccessBase access = new SyncAccess(server, 500);
            // 这是个回调函数，就是读到值后执行再执行下面的代码，是用匿名类写的，当然也可以写到外面去
            access.addItem(itemId, new DataCallback() {
                @Override
                public void changed(Item item, ItemState state) {

                }
            });

//            // Add a new group，添加一个组，这个用来就读值或者写值一次，而不是循环读取或者写入
//            // 组的名字随意，给组起名字是因为，server可以addGroup也可以removeGroup，读一次值，就先添加组，然后移除组，再读一次就再添加然后删除
//            final Group group = server.addGroup("test");
//            // Add a new item to the group，
//            // 将一个item加入到组，item名字就是MatrikonOPC Server或者KEPServer上面建的项的名字比如：u.u.TAG1，PLC.S7-300.TAG1
//            final Item item = group.addItem(itemId);

            // start reading，开始循环读值
            access.bind();

            // stop reading，停止循环读取数值
            access.unbind();
        } catch (final JIException | UnknownHostException | NotConnectedException | DuplicateGroupException | AlreadyConnectedException | AddFailedException e) {
            System.out.println(String.format("异常信息为: %s", e.getLocalizedMessage()));
        }
    }

//    public static void main(String[] args)  throws Exception{
//        ConnectionInformation connectionInformation = connection();
////        readValue(connectionInformation,"JDSimulate.JDAOTPlc.AOT.Result");
//        readValueSchedule(connectionInformation,"JDSimulate.JDAOTPlc.AOT.Result");
//    }
}
