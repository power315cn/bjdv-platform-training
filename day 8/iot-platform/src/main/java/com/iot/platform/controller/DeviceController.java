package com.iot.platform.controller;

import com.iot.platform.model.Device;
import com.iot.platform.model.DeviceQuery;
import com.iot.platform.service.DeviceService;
import com.iot.platform.util.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @desc:   设备管理控制器
 * @author: michael
 * @date:2021/7/16 9:41
 */
@RestController
@RequestMapping("device")
public class DeviceController {

    @Autowired
    private DeviceService deviceService;

    @PostMapping
    public ResponseDto addDevice(@RequestBody Device device){
      Long id =  deviceService.addDevice(device);
      return new ResponseDto(1,"创建成功",id);
    }

    @PatchMapping
    public ResponseDto updateDevice(@RequestBody Device device){
        Long id =  deviceService.updateDevice(device);
        return new ResponseDto(1,"修改成功",id);
    }

    @DeleteMapping("/{code}")
    public ResponseDto deleteDevice(@PathVariable("code") String code){
        deviceService.delete(code);
        return new ResponseDto(1,"删除成功",null);
    }

    @GetMapping("/{code}")
    public ResponseDto getByCode(@PathVariable("code") String code){
        return new ResponseDto(1,"根据编码获取设备成功",deviceService.getByDeviceCode(code));
    }



    @GetMapping("list")
    public ResponseDto list(DeviceQuery deviceQuery){
        return new ResponseDto(1,"获取列表成功",deviceService.getListByQuery(deviceQuery));
    }

}
