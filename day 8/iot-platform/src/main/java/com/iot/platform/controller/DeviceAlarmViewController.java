package com.iot.platform.controller;

import com.iot.platform.model.DeviceQuery;
import com.iot.platform.service.DeviceAlarmViewService;
import com.iot.platform.util.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @desc:
 * @author: michael
 * @date:2021/7/19 15:17
 */
@RestController
@RequestMapping("deviceAlarmView")
public class DeviceAlarmViewController {


    @Autowired
    private DeviceAlarmViewService deviceAlarmViewService;

    @GetMapping("view")
    private ResponseDto view(DeviceQuery deviceQuery){
        return new ResponseDto(1,"获取列表信息成功",deviceAlarmViewService.getPointInfo(deviceQuery));
    }


}
