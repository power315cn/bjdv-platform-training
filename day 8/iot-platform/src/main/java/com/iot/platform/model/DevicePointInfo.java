package com.iot.platform.model;

import java.util.Map;

/**
 * @desc:   设备点位信息
 * @author: michael
 * @date:2021/7/19 15:25
 */
public class DevicePointInfo {

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备信息
     */
    private Map<String,Object> info;

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public Map<String, Object> getInfo() {
        return info;
    }

    public void setInfo(Map<String, Object> info) {
        this.info = info;
    }

    @Override
    public String toString() {
        return "DevicePointInfo{" +
                "deviceName='" + deviceName + '\'' +
                ", deviceCode='" + deviceCode + '\'' +
                ", info=" + info +
                '}';
    }
}
