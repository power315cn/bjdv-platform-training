package com.iot.platform.enums;

/**
 * @desc: 协议类型枚举类
 * @author: michael
 * @date:2021/7/20 10:09
 */
public enum  SslTypeEnum {

    KEPWARE(1,"Kepware Server","Kepware Server");

    public  Integer code;

    public final String value;

    public final String remark;

    SslTypeEnum(Integer code,String value,String remark){
        this.remark = remark;
        this.value=value;
        this.code =code;
    }
}
