package com.iot.platform.model;

/**
 * @desc:   设备采集点查询
 * @author: michael
 * @date:2021/7/15 14:06
 */
public class DeviceCaputerPointQuery {

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 采集名称
     */
    private String caputerPointName;

    /**
     * 寄存器地址
     */
    private String registerAddress;

    public DeviceCaputerPointQuery() {
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getCaputerPointName() {
        return caputerPointName;
    }

    public void setCaputerPointName(String caputerPointName) {
        this.caputerPointName = caputerPointName;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    @Override
    public String toString() {
        return "DeviceCaputerPointQuery{" +
                "deviceCode='" + deviceCode + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", caputerPointName='" + caputerPointName + '\'' +
                ", registerAddress='" + registerAddress + '\'' +
                '}';
    }
}
