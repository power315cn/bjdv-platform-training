package com.iot.platform.service;

import com.iot.platform.mapper.DeviceCaputerPointMapper;
import com.iot.platform.model.DeviceCaputerPoint;
import com.iot.platform.model.DeviceCaputerPointQuery;
import org.apache.logging.log4j.util.Strings;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @desc:   设备采集点服务
 * @author: michael
 * @date:2021/7/15 17:36
 */
@Service
public class DeviceCaputerPointService {

    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceCaputerPointService.class);

    @Autowired
    private DeviceCaputerPointMapper deviceCaputerPointMapper;

    @Autowired
    private DeviceService deviceService;

    /**
     * 新增设备采集点
     * @param deviceCaputerPoint
     * @return
     */
    public Long addDeviceCaputerPoint(DeviceCaputerPoint deviceCaputerPoint){
        LOGGER.info("新增设备采集点[addDeviceCaputerPoint]<START>,参数信息为:[{}]",deviceCaputerPoint);
        //校验参数
        checkParam(deviceCaputerPoint);
        DeviceCaputerPoint deviceCaputerPointOld = deviceCaputerPointMapper.getByCode(deviceCaputerPoint.getCaputerPointCode());
        if(null != deviceCaputerPointOld){
            throw new RuntimeException("采集编号已经存在");
        }
        //新增对象
        deviceCaputerPointMapper.addDeviceCaputerPoint(deviceCaputerPoint);
        return deviceCaputerPoint.getId();
    }

    /**
     * 修改
     * @param deviceCaputerPoint
     * @return
     */
    public Long updateDeviceCaputerPoint(DeviceCaputerPoint deviceCaputerPoint){
        //校验参数
        checkParam(deviceCaputerPoint);
        //获取对象
        DeviceCaputerPoint deviceCaputerPointOld = deviceCaputerPointMapper.getByCode(deviceCaputerPoint.getCaputerPointCode());
        if(null == deviceCaputerPointOld){
            throw new RuntimeException("采集点对象已删除或已被修改");
        }
        deviceCaputerPoint.setId(deviceCaputerPointOld.getId());
        deviceCaputerPoint.setVersion(deviceCaputerPointOld.getVersion());
        deviceCaputerPointMapper.updateDeviceCaputerPoint(deviceCaputerPoint);
        return deviceCaputerPoint.getId();
    }

    /**
     * 根据编码获取采集点对象
     * @param code  采集点编码
     * @return
     */
    public DeviceCaputerPoint getByCode(String code){
        if(Strings.isBlank(code)){
            throw new RuntimeException("采集编号不能为空");
        }
        return deviceCaputerPointMapper.getByCode(code);
    }

    /**
     * 根据采集点编码删除采集点
     * @param code
     */
    public void deleteByCode(String code){
        if(Strings.isBlank(code)){
            throw new RuntimeException("采集编号不能为空");
        }
        deviceCaputerPointMapper.deleteByCode(code);
    }

    /**
     * 根据查询条件获取采集点列表
     * @param deviceCaputerPointQuery   采集点查询对象
     * @return
     */
    public List<DeviceCaputerPoint> getByQuery(DeviceCaputerPointQuery deviceCaputerPointQuery){
        return deviceCaputerPointMapper.getByQuery(deviceCaputerPointQuery);
    }



    private void checkParam(DeviceCaputerPoint deviceCaputerPoint) {
        if(null == deviceCaputerPoint){
            throw new RuntimeException("设备采集点信息不能为空");
        }
        if(Strings.isBlank(deviceCaputerPoint.getDeviceCode()) || Strings.isBlank(deviceCaputerPoint.getDeviceName())){
            throw new RuntimeException("设备信息不能为空");
        }
        if(Strings.isBlank(deviceCaputerPoint.getCaputerPointCode())){
            throw new RuntimeException("采集编号不能为空");
        }
        if(Strings.isBlank(deviceCaputerPoint.getCaputerPointName())){
            throw new RuntimeException("采集名称不能为空");
        }
        if(null == deviceCaputerPoint.getDataType()){
            throw new RuntimeException("数据类型不能为空");
        }
        if(Strings.isBlank(deviceCaputerPoint.getRegisterAddress())){
            throw new RuntimeException("寄存地址不能为空");
        }
        if(null == deviceService.getByDeviceCode(deviceCaputerPoint.getDeviceCode())){
            throw new RuntimeException("设备信息不存在");
        }
    }


}
