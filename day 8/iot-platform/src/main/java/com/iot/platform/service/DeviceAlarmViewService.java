package com.iot.platform.service;

import com.iot.platform.enums.DataTypeEnum;
import com.iot.platform.model.DeviceDetail;
import com.iot.platform.model.DeviceQuery;
import com.iot.platform.util.OpcConnection;
import com.iot.platform.util.OpcUtil;
import org.apache.logging.log4j.util.Strings;
import org.jinterop.dcom.common.JIException;
import org.openscada.opc.lib.common.AlreadyConnectedException;
import org.openscada.opc.lib.common.NotConnectedException;
import org.openscada.opc.lib.da.DuplicateGroupException;
import org.openscada.opc.lib.da.Group;
import org.openscada.opc.lib.da.Server;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.net.UnknownHostException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.stream.Collectors;

/**
 * @desc:   设备告警服务
 * @author: michael
 * @date:2021/7/19 15:18
 */
@Service
public class DeviceAlarmViewService {


    private static final Logger LOGGER = LoggerFactory.getLogger(DeviceAlarmViewService.class);

    @Autowired
    private DeviceService deviceService;//设备服务

    public List<DeviceDetail> getPointInfo(DeviceQuery deviceQuery){

        List<DeviceDetail> deviceDetails = deviceService.listDeviceDetail(deviceQuery);
        LOGGER.info("获取设备详细数据为:[{}]",deviceDetails);
        //循环遍历获取所有寄存器地址
        if(null != deviceDetails && deviceDetails.size()>0){
            List<String> itemIds = deviceDetails.stream().filter(v->v.getDeviceCaputerPointAndValues()!=null)
                    .flatMap(v->v.getDeviceCaputerPointAndValues().stream()).filter(v-> !Strings.isEmpty(v.getRegisterAddress())).map(c->c.getRegisterAddress()).collect(Collectors.toList());

            LOGGER.info("点位信息为:[{}]",itemIds);
            //创建KEPWARE连接
            Map<String,String> dataMap = getData(itemIds);
            LOGGER.info("获取kepware数据为:[{}]",dataMap);
            if(!(null == dataMap || dataMap.size()==0)){
                deviceDetails.stream().filter(v->v.getDeviceCaputerPointAndValues()!=null)
                        .flatMap(v->v.getDeviceCaputerPointAndValues().stream())
                        .filter(v-> !Strings.isEmpty(v.getRegisterAddress())).forEach(v->{
                            if(DataTypeEnum.BOOLEAN.value.equals(v.getDataType())){
                                v.setValue(Boolean.valueOf(dataMap.get(v.getRegisterAddress())));
                            }
                            if(DataTypeEnum.STRING.value.equals(v.getDataType())){
                                v.setValue(dataMap.get(v.getRegisterAddress()));
                            }
                            if(DataTypeEnum.FLOAT.value.equals(v.getDataType())){
                                v.setValue(Float.valueOf(dataMap.get(v.getRegisterAddress())));
                            }
                            if(DataTypeEnum.INTEGER.value.equals(v.getDataType())){
                                v.setValue(Integer.valueOf(dataMap.get(v.getRegisterAddress())));
                            }
                });
            }
        }
        return deviceDetails;
    }

    private  Map<String,String> getData(List<String> ITEMS) {
        Server server = new Server(OpcConnection.getInfo(), Executors.newSingleThreadScheduledExecutor());
        Map<String,String> result = new HashMap<String,String>();
        try {
            server.connect();
            Group group = server.addGroup();
            result = OpcUtil.readValuesMap(group,ITEMS);
        } catch (JIException | NotConnectedException | DuplicateGroupException | UnknownHostException | AlreadyConnectedException e) {
            e.printStackTrace();
        } finally {
            server.disconnect();
        }
        result.forEach((k,v)->{
            System.out.println(k+"@@"+v);
        });
        return result;
    }
}
