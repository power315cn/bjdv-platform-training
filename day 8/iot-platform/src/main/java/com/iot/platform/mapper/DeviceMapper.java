package com.iot.platform.mapper;

import com.iot.platform.model.Device;
import com.iot.platform.model.DeviceDetail;
import com.iot.platform.model.DeviceQuery;

import java.util.List;

/**
 * @desc: 设备Mapper
 * @author: michael
 * @date:2021/7/15 11:18
 */
public interface DeviceMapper {

    /**
     * 新增设备
     * @param device 设备对象
     */
    void addDevice(Device device);

    /**
     * 修改设备信息
     * @param device    设备对象
     */
    void updateDevice(Device device);

    /**
     * 根据参数查询设备信息
     * @param deviceQuery   设备参数
     * @return
     */
    List<Device> listDevice(DeviceQuery deviceQuery);

    /**
     * 根据设备编码删除设备信息
     * @param deviceCode    设备编码
     */
    void deleteDevice(String deviceCode);

    /**
     * 根据设备编码获取设备信息
     * @param deviceCode    设备编码
     * @return
     */
    Device getByDeviceCode(String deviceCode);

    /**
     * 根据条件查询设备信息列表
     * @param deviceQuery
     * @return
     */
    List<DeviceDetail> listDeviceDetail(DeviceQuery deviceQuery);

}
