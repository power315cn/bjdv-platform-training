package com.iot.platform.mapper;

import com.iot.platform.model.DeviceCaputerPoint;
import com.iot.platform.model.DeviceCaputerPointAndValue;
import com.iot.platform.model.DeviceCaputerPointQuery;

import java.util.List;

/**
 * @desc:   设备采集点管理
 * @author: michael
 * @date:2021/7/15 14:44
 */
public interface DeviceCaputerPointMapper {

    /**
     * 新增设备采集点
     * @param deviceCaputerPoint
     */
    void addDeviceCaputerPoint(DeviceCaputerPoint deviceCaputerPoint);

    /**
     * 修改设备采集点
     * @param deviceCaputerPoint
     */
    void updateDeviceCaputerPoint(DeviceCaputerPoint deviceCaputerPoint);

    /**
     * 根据ID删除采集点
     * @param code
     */
    void deleteByCode(String code);

    /**
     * 根据设备编码或采集点信息
     * @param deviceCaputerPointQuery
     * @return
     */
    List<DeviceCaputerPoint> getByQuery(DeviceCaputerPointQuery deviceCaputerPointQuery);

    /**
     * 根据编码获取对象
     * @param caputerPointCode
     * @return
     */
    DeviceCaputerPoint getByCode(String caputerPointCode);

    /**
     * 根据设备编码获取采集点信息
     * @param deviceCode
     * @return
     */
    List<DeviceCaputerPointAndValue> getByDeviceCode(String deviceCode);

}
