package com.iot.platform.model;

import java.io.Serializable;
import java.util.Date;

/**
 * @desc:   设备采集点管理
 * @author: michael
 * @date:2021/7/15 11:04
 */
public class DeviceCaputerPoint implements Serializable {

    /**
     * 主键
     */
    private Long id;

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    /**
     * 采集编号
     */
    private String caputerPointCode;

    /**
     * 采集名称
     */
    private String caputerPointName;

    /**
     * 数据类型
     */
    private Integer dataType;

    /**
     * 寄存器地址
     */
    private String registerAddress;

    /**
     * 备注
     */
    private String remark;

    /**
     * 创建时间
     */
    private Date createTime;

    /**
     * 创建者
     */
    private String creator;

    /**
     * 修改时间
     */
    private Date updateTime;

    /**
     * 修改者
     */
    private String updator;
    /**
     * 版本信息
     */
    private Long version;

    public DeviceCaputerPoint() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    public String getCaputerPointCode() {
        return caputerPointCode;
    }

    public void setCaputerPointCode(String caputerPointCode) {
        this.caputerPointCode = caputerPointCode;
    }

    public String getCaputerPointName() {
        return caputerPointName;
    }

    public void setCaputerPointName(String caputerPointName) {
        this.caputerPointName = caputerPointName;
    }

    public Integer getDataType() {
        return dataType;
    }

    public void setDataType(Integer dataType) {
        this.dataType = dataType;
    }

    public String getRegisterAddress() {
        return registerAddress;
    }

    public void setRegisterAddress(String registerAddress) {
        this.registerAddress = registerAddress;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

    public Date getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Date createTime) {
        this.createTime = createTime;
    }

    public String getCreator() {
        return creator;
    }

    public void setCreator(String creator) {
        this.creator = creator;
    }

    public Date getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Date updateTime) {
        this.updateTime = updateTime;
    }

    public String getUpdator() {
        return updator;
    }

    public void setUpdator(String updator) {
        this.updator = updator;
    }

    @Override
    public String toString() {
        return "DeviceCaputerPoint{" +
                "id=" + id +
                ", deviceCode='" + deviceCode + '\'' +
                ", deviceName='" + deviceName + '\'' +
                ", caputerPointCode='" + caputerPointCode + '\'' +
                ", caputerPointName='" + caputerPointName + '\'' +
                ", dataType='" + dataType + '\'' +
                ", registerAddress='" + registerAddress + '\'' +
                ", remark='" + remark + '\'' +
                ", createTime=" + createTime +
                ", creator='" + creator + '\'' +
                ", updateTime=" + updateTime +
                ", updator='" + updator + '\'' +
                ", version=" + version +
                '}';
    }
}
