package com.iot.platform.model;

/**
 * @desc:   设备查询参数
 * @author: michael
 * @date:2021/7/15 14:01
 */
public class DeviceQuery {

    /**
     * 设备编码
     */
    private String deviceCode;

    /**
     * 设备名称
     */
    private String deviceName;

    public DeviceQuery() {
    }

    public String getDeviceCode() {
        return deviceCode;
    }

    public void setDeviceCode(String deviceCode) {
        this.deviceCode = deviceCode;
    }

    public String getDeviceName() {
        return deviceName;
    }

    public void setDeviceName(String deviceName) {
        this.deviceName = deviceName;
    }

    @Override
    public String toString() {
        return "DeviceQuery{" +
                "deviceCode='" + deviceCode + '\'' +
                ", deviceName='" + deviceName + '\'' +
                '}';
    }
}
