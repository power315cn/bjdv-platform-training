package com.iot.platform.controller;

import com.iot.platform.model.DeviceCaputerPoint;
import com.iot.platform.model.DeviceCaputerPointQuery;
import com.iot.platform.service.DeviceCaputerPointService;
import com.iot.platform.util.ResponseDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @desc:
 * @author: michael
 * @date:2021/7/16 10:14
 */
@RestController
@RequestMapping("deviceCaputerPoint")
public class DeviceCaputerPointController {

    @Autowired
    private DeviceCaputerPointService deviceCaputerPointService;

    @PostMapping
    public ResponseDto addDeviceCaputerPointService(@RequestBody DeviceCaputerPoint deviceCaputerPoint){
        Long id =  deviceCaputerPointService.addDeviceCaputerPoint(deviceCaputerPoint);
        return new ResponseDto(1,"创建成功",id);
    }

    @PatchMapping
    public ResponseDto updateDeviceCaputerPoint(@RequestBody DeviceCaputerPoint deviceCaputerPoint){
        Long id =  deviceCaputerPointService.updateDeviceCaputerPoint(deviceCaputerPoint);
        return new ResponseDto(1,"修改成功",id);
    }

    @DeleteMapping("/{code}")
    public ResponseDto deleteDeviceCaputerPoint(@PathVariable("code") String code){
        deviceCaputerPointService.deleteByCode(code);
        return new ResponseDto(1,"删除成功",null);
    }

    @GetMapping("/{code}")
    public ResponseDto getByCode(@PathVariable("code") String code){
        deviceCaputerPointService.getByCode(code);
        return new ResponseDto(1,"删除成功",null);
    }



    @GetMapping("list")
    public ResponseDto list(DeviceCaputerPointQuery deviceCaputerPointQuery){
        return new ResponseDto(1,"获取列表成功",deviceCaputerPointService.getByQuery(deviceCaputerPointQuery));
    }

}
