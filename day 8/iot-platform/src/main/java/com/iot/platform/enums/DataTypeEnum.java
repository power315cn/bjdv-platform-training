package com.iot.platform.enums;

/**
 * 数据类型枚举类
 */
public enum DataTypeEnum {

    STRING(4,"4","String"),

    FLOAT(3,"3","Float"),

    BOOLEAN(2,"2","Boolean"),

    INTEGER(1,"1","Integer");

    public  Integer code;

    public final String value;

    public final String remark;

    DataTypeEnum(Integer code,String value,String remark){
        this.remark = remark;
        this.value=value;
        this.code =code;
    }
}
