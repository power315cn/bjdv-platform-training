-- 1、数据库的基本操作
-- 查看或显示数据库
SHOW DATABASES;

-- 创建数据库
CREATE DATABASE IF NOT EXISTS druid DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- 修改数据库
ALTER DATABASE druid DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

-- 删除数据库
DROP DATABASE druid;

-- 选择数据库
USE druid;

-- 2、表的基本操作
-- 创建数据表
CREATE TABLE `students` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`sn` VARCHAR ( 32 ) NOT NULL COMMENT '学号',
	`name` VARCHAR ( 32 ) NOT NULL COMMENT '名称',
	`age` INT ( 11 ) NOT NULL COMMENT '年龄',
    `major` varchar(32) NOT NULL COMMENT '专业方向',
	PRIMARY KEY ( `id` ) USING BTREE
);

-- 修改数据表
ALTER TABLE students RENAME TO student;

-- 修改字段名称
ALTER TABLE student CHANGE `sn` `number` varchar(32) NOT NULL COMMENT '学号';

-- 删除数据表
DROP TABLE student;

-- 添加数据表字段
ALTER TABLE student ADD sex varchar(2) NOT NULL COMMENT '性别';

-- 3、表数据的基本操作
-- 查询语句
SELECT * FROM student;
SELECT * FROM student ORDER BY number;
SELECT * FROM student ORDER BY number LIMIT 10;

-- 插入数据
INSERT INTO `student` ( `number`, `name`, `age`, `major`, `sex` )
VALUES ( '182208203118', '郑艺旋', 22, '软件工程', '女' );

-- 修改数据
UPDATE student SET age = 18 WHERE id = 1;

-- 删除数据
DELETE FROM student WHERE id =1;

-- 初始数据
CREATE TABLE `student` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
  `number` varchar(32) NOT NULL COMMENT '学号',
  `name` varchar(32) NOT NULL COMMENT '名称',
  `age` int(11) NOT NULL COMMENT '年龄',
  `major` varchar(32) NOT NULL COMMENT '专业方向',
  `sex` varchar(2) NOT NULL COMMENT '性别',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `student` ( `number`, `name`, `age`, `major`, `sex` )
VALUES
	( '182208203101', '刘子杰', 22, '软件工程', '男' ),
	( '182208203150', '陈旭辉', 22, '数据科学', '男' ),
	( '182208206118', '何旭', 22, '网络工程', '男' ),
	( '182208206104', '周孙沁怡', 22, '网络工程', '男' ),
	( '182208203124', '王宜辉', 22, '软件工程', '男' ),
	( '182208202145', '余湘', 22, '软件工程', '男' ),
	( '182208203103', '罗坤', 22, '软件工程', '男' ),
	( '182208204101', '望远承', 22, '数据科学', '男' ),
	( '182212101144', '彭思博', 22, '软件工程', '男' ),
	( '182208202146', '毛慧', 22, '软件工程', '女' ),
	( '182208204111', '张宇鹏', 22, '数据科学', '男' ),
	( '182208203118', '郑艺旋', 22, '软件工程', '女' ),
	( '182208203144', '姚诗雨', 22, '数据科学', '女' ),
	( '182208203107', '李轶杰', 22, '软件工程', '男' ),
	( '182208204102', '潘兴宇', 22, '软件工程', '男' ),
	( '182208204150', '王嘉伟', 22, '数据科学', '男' ),
	( '182208204131', '高诗婕', 22, '软件工程', '女' ),
	( '182208203149', '刘昊', 22, '数据科学', '男' ),
	( '182208202140', '熊婧', 22, '软件工程', '女' ),
	( '182208203111', '毛晓楠', 22, '软件工程', '女' ),
	( '182208203146', '甘丽华', 22, '软件工程', '女' ),
	( '182208204108', '汪靖程', 22, '数据科学', '男' ),
	( '182208204112', '王永舒', 22, '数据科学', '男' );


CREATE TABLE `account` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`student_id` INT ( 11 ) NOT NULL COMMENT '学生表的id',
	`balance` DECIMAL ( 10, 2 ) NOT NULL COMMENT '账号存款',
	PRIMARY KEY ( `id` ) USING BTREE,
    UNIQUE KEY `UQ_student_id` (`student_id`) USING BTREE
);

INSERT INTO `account` ( `student_id`, `balance` )
VALUES
	( 1, 100.00 ),
	( 2, 100.00 ),
	( 3, 100.00 ),
	( 4, 100.00 ),
	( 5, 100.00 ),
	( 6, 100.00 ),
	( 7, 100.00 ),
	( 8, 100.00 ),
	( 9, 100.00 ),
	( 10, 100.00 ),
	( 11, 100.00 ),
	( 12, 100.00 ),
	( 13, 100.00 ),
	( 14, 100.00 ),
	( 15, 100.00 ),
	( 16, 100.00 ),
	( 17, 100.00 ),
	( 18, 100.00 ),
	( 19, 100.00 ),
	( 20, 100.00 ),
	( 21, 100.00 ),
	( 22, 100.00 );
-- 	( 23, 100.00 );

-- 内连接
SELECT s.*,a.balance FROM student s INNER JOIN account a ON s.id=a.student_id;

-- 左连接
SELECT s.*,a.balance FROM student s LEFT JOIN account a ON s.id=a.student_id;

-- 右连接
SELECT s.*,a.balance FROM student s RIGHT JOIN account a ON s.id=a.student_id;

-- 4、复杂查询
-- 统计每个专业的人数
SELECT major, count(*) as count FROM student GROUP BY major;

-- 查询专业人数大于5个人的记录
SELECT major, count(*) as count FROM student GROUP BY major HAVING count >5;

-- 5、事务
-- 读未提交
-- 1）打开一个事务A，设置当前事务模式为读未提交，查询表account的初始值：100
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- 开启事务
    START TRANSACTION;
    -- 查看当前数据
    SELECT * FROM account;

-- 2）打开另一个事务B，更新表account字段balance：100-10
    SET SESSION TRANSACTION ISOLATION LEVEL READ UNCOMMITTED;
    -- 开启事务
    START TRANSACTION;
    -- 更新balance
    UPDATE account SET balance = balance - 10 WHERE id = 1;

-- 3）事务B的事务还没提交，但是事务A就可以查询到B已经更新的数据：90
    SELECT * FROM account;

-- 4）一旦事务B的事务因为某种原因回滚，所有的操作都将会被撤销，那事务A查询到的数据其实就是脏数据：90（实际上数据回滚之后还是100）

-- 读已提交
-- 1）打开一个事务A，并设置当前事务模式为read committed，查询表account的所有记录：100
SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
    -- 开启事务
    START TRANSACTION;
    -- 查看当前数据
    SELECT * FROM account;

-- 2）在事务A的事务提交之前，打开另一个事务B，更新表account：100-10
    SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED;
    -- 开启事务
    START TRANSACTION;
    -- 更新balance
    UPDATE account SET balance = balance - 10 WHERE id = 1;

-- 3）这时，事务B的事务还没提交，事务A不能查询到B已经更新的数据，解决了脏读问题：100
    SELECT * FROM account;

-- 4）事务B的事务提交
    COMMIT;

-- 5）事务A在事务B提交后，查询到改变的结果：90
    SELECT * FROM account;

-- 可重复读
-- 1）打开一个事务A，并设置当前事务模式为可重复读，查询表account的所有记录：90
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
    -- 开启事务
    START TRANSACTION;
    -- 查看当前数据
    SELECT * FROM account;

-- 2）在事务A的事务提交之前，打开另一个事务B，更新表account：90-10
    SET SESSION TRANSACTION ISOLATION LEVEL REPEATABLE READ;
    -- 开启事务
    START TRANSACTION;
    -- 更新balance
    UPDATE account SET balance = balance - 10 WHERE id = 1;

-- 3）这时，事务B的事务还没提交，事务A不能查询到B已经更新的数据：90
    SELECT * FROM account;

-- 4）事务B的事务提交
    COMMIT;

-- 5）事务A查询到的结果没有变：90，在一个事务中数据是保持一致的
    SELECT * FROM account;

-- 串行化
-- 1）打开一个事务A，并设置当前事务模式为串行化，查询表account的初始值：
    SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    -- 开启事务
    START TRANSACTION;
    -- 查询当前记录
    SELECT * FROM account;

-- 2）打开一个事务B，并设置当前事务模式为串行化，插入一条记录报错，表被锁了插入失败
    SET SESSION TRANSACTION ISOLATION LEVEL SERIALIZABLE;
    -- 开启事务
    START TRANSACTION;
    -- 查询当前记录
    SELECT * FROM account;
    -- 更新balance失败，表被锁住了
    UPDATE account SET balance = balance - 10 WHERE id = 1;


-- 问题：
-- 1、查询姓王的同学记录（student表）。
SELECT * FROM student WHERE name LIKE '王%';

-- 2、根据性别分组查询的数量，显示数量，性别字段（student表）。
SELECT count(*) as count, sex FROM student GROUP BY sex ;

-- 3、统计每个专业方向男女人数（student表）。
SELECT major,sex,count(*) as count FROM student GROUP BY major,sex;

-- 4、创建用户表user
-- 字段主建id，用户名username，密码password，姓名name，创建时间create_time，修改时间update_time；
CREATE TABLE `user` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`username` VARCHAR ( 32 ) NOT NULL COMMENT '用户名',
	`password` VARCHAR ( 32 ) NOT NULL COMMENT '密码',
	`name` VARCHAR ( 32 ) NOT NULL COMMENT '姓名',
	`create_time` datetime DEFAULT NULL COMMENT '创建时间',
	`update_time` datetime DEFAULT NULL COMMENT '修改时间',
	PRIMARY KEY ( `id` ) USING BTREE,
    UNIQUE KEY `UQ_username` ( `username` ) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;


-- 5、创建操作日志表operate_log
-- 字段主键id，操作动作action，操作内容content，操作人operator，操作时间operate_time
CREATE TABLE `operate_log` (
	`id` INT ( 11 ) NOT NULL AUTO_INCREMENT COMMENT '自增主键',
	`action` VARCHAR ( 32 ) NOT NULL COMMENT '操作：查询，分页，新增，修改，删除，登录，注销',
	`content` VARCHAR ( 128 ) NOT NULL COMMENT '操作内容',
	`operator` VARCHAR ( 32 ) NOT NULL COMMENT '操作人',
	`operate_time` datetime DEFAULT NULL COMMENT '操作时间',
	PRIMARY KEY ( `id` ) USING BTREE
) ENGINE = INNODB DEFAULT CHARSET = utf8mb4;


